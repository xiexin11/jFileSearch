package net.qqxh.service;

import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import org.jodconverter.office.OfficeException;

import java.io.File;
import java.io.IOException;

public interface FileResolveService {
    String resolveFile2Text(File file, String type) throws IOException;



    boolean resolve(SearchLib searchLib, Jfile file) throws IOException, OfficeException;

    String resolveFile2View(String filePath, SearchLib searchLib, String type) throws IOException, OfficeException;

    void deleteFileFromRedis(String rid);
}
