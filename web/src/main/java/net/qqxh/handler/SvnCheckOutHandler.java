package net.qqxh.handler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.ISVNEventHandler;
import org.tmatesoft.svn.core.wc.SVNEvent;
import org.tmatesoft.svn.core.wc.SVNEventAction;


/**
 * Created by jason on 2019/2/14 10:44
 *
 * @author jason
 */

public class SvnCheckOutHandler implements ISVNEventHandler {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private WorkSuccessCallBack workSuccessCallBack;

    public interface WorkSuccessCallBack {
        void callback();
    }

    public  SvnCheckOutHandler(WorkSuccessCallBack callBack) {
        this.workSuccessCallBack = callBack;
    }
    @Override
    public void handleEvent(SVNEvent svnEvent, double v) throws SVNException {
        SVNEventAction action = svnEvent.getAction();
        logger.info("update success ：" + action.toString() + svnEvent.getFile().getPath());

        if (action.getID() == SVNEventAction.UPDATE_COMPLETED.getID()) {
            workSuccessCallBack.callback();
        }

    }
    @Override
    public void checkCancelled() throws SVNCancelException {

    }

}
